package ru.tsc.karbainova.tm.model;

import ru.tsc.karbainova.tm.enumerated.Status;

import java.util.UUID;

public class Task {

    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private Status status = Status.NOT_STARTED;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + " " + name + " ";
    }
}
