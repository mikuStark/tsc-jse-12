package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByIndex(int index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);


}
